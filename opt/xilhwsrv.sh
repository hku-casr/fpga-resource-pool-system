#!/bin/bash

#
# Name: xilhwsrv.sh
#
# Description: This script starts or kills Xilinx Vivado Hardware Server for a
#  JTAG device. The script will kickstart or terminate Hardware Servers of
#  every Vivado version it found based on the Vivado base path coded.
#
#  The script will attempt to read from a colon-separated value file at
#  /<clustername>/<hostname>/etc/xilhwsrv_port that holds a list of configured
#  port for a given JTAG device and Vivado version.
#
#  If the script has not been found to exist or the listed JTAG is not listed,
#  the script will attempt to find the lowest possible ports, closest to the
#  default hardware server port 3121 to assign to Hardware Servers to be
#  started. The resulting assignments will be written back to the named file
#  at /<clustername>/<hostname>/etc/xilhwsrv_port for next configuration.
#
#  The script uses dummy accounts to own individual JTAG device and own the
#  Xilinx Vivado Hardware Server so that the Hardware Server can only see a
#  single JTAG device. If a dummy account is not found for a JTAG device, the
#  script will create such dummy account.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

usage="Start or Kill Vivado Hardware Server for FPGA
Usage: $0 <jtag_esn> ( 'start' | 'stop' | 'restart' )
 e.g.: $0 jtag_00001234567890 restart";

if [ ! $# -eq 2 ]; then
    echo "$usage";

    exit 1;
else
    jtag=$1;

    if [ "$2" == "start" ] || [ "$2" == "restart" ]; then
        nact=$2;
    elif [ "$2" == "stop" ] || [ "$2" == "kill" ]; then
        nact="kill";
    else
        echo "$usage";

        exit 1;
    fi;
fi;

# THIS IS A SCRIPT THAT MIGHT BE CALLED AT INIT, CLUSTERNAME IS NOT AVAILABLE
#  AT INIT PROCESS, AS CLUSTERNAME IS POPULATED WITH /ETC/PROFILE.
#
if [ -z "$clus" ]; then
    source /$CLUSTERNAME/opt/common.sh;
else
    source /$clus/opt/common.sh;
fi;

xilv="/opt/Xilinx/Vivado"
base="/$clus/$host/var/lock";
pfile="/$clus/`echo $HOSTNAME | cut -d "." -f1`/etc/xilhwsrv_port";
baseport=$xilhwsrv_baseport;
portbound=$xilhwsrv_portbound;
count=0;

# KILL HARDWARE SERVER FOR EVERY VERSION FOUND IN LOCAL NODE
#
if [ "$nact" == "kill" ] || [ "$nact" == "restart" ]; then
    echo "INFO: Killing Running Hardware Servers for $jtag";

    if [ -e $pfile ]; then
        while read line; do
            if [ `echo $line | cut -d ':' -f1` == "$jtag" ]; then
                vver=$(echo $line | cut -d ':' -f2);
                port=$(echo $line | cut -d ':' -f3);

                proc=$(ss -lp "( sport = :$port )" | grep "LISTEN" | tr -s " " | cut -d " " -f6 | cut -d ":" -f2 | cut -d "," -f2);

                if [ ! -z "$proc" ]; then
                    echo "Killing Vivado Hardware Server ($proc) Version $vver at Port $port";

                    kill -n 9 $proc;
                fi;
            fi;
        done < $pfile;
    fi;
fi;

# START HARDWARE SERVER FOR EVERY VERSION FOUND IN LOCAL NODE FROM LATEST
#  TO OLDEST.
#
if [ "$nact" == "start" ] || [ "$nact" == "restart" ]; then
    echo "INFO: Starting Hardware Servers for $jtag";

    jtag_user="nobody_$jtag";

    if [ `getent passwd | grep "nobody_$jtag" | wc -l` -eq 0 ]; then
        echo "Next Stage: Create Dummy JTAG Owner User $jtag_user";

        useradd -Md / -c "Nobody $jtag" -Ng nobody -s /sbin/nologin -K UID_MIN=3000 -K UID_MAX=3100 $jtag_user;
    fi;

    chmod 600 $base/$1/usb_jtag;
    chown $jtag_user $base/$1/usb_jtag;

    for i in `ls -r $xilv`; do
        vver=$i;

        # CHECK IF THE PORT CONFIGURATION FILE EXISTS AND THE CORRESPONDING RULE
        #  HAS BEEN FOUND.
        #
        if [ -e $pfile ] \
            && [ `cat $pfile | grep "$jtag" | grep "$vver" | wc -l` -gt 0 ]; then
            while read line; do
                if [ `echo $line | cut -d ':' -f1` == "$jtag" ] \
                    && [ `echo $line | cut -d ':' -f2` == "$vver" ]; then
                    port=$(echo $line | cut -d ':' -f3);

                    echo "Starting Vivado Hardware Server Version $vver at Port $port";

                    sudo -u $jtag_user -g nobody $xilv/$vver/bin/hw_server -d -s tcp::$port > /dev/null;

                    echo "INFO: Vivado Hardware Server $vver for JTAG ESN $jtag is Listening at $host:$port";
                fi;
            done < $pfile;
        else
            # LOOK FOR SMALLEST UNBOUND PORT
            #
            while [ `ss -nl "( sport = :$((baseport+count)) )" | grep "LISTEN" | wc -l` -gt 0 ]; do
                count=$((count+1));
            done;

            port=$((baseport+count));

            if [ "$port" -ge "$portbound" ]; then
                echo "FAIL: TCP Port Out of Bound.";

                exit 1;
            fi;

            echo "Starting Vivado Hardware Server Version $vver at Port $port";

            sudo -u $jtag_user -g nobody $xilv/$vver/bin/hw_server -d -s tcp::$port > /dev/null;

            echo "INFO: Vivado Hardware Server $vver for JTAG ESN $jtag is Listening at $host:$port";

            echo "$jtag:$i:$port" >> $pfile;
        fi;
    done;
fi;