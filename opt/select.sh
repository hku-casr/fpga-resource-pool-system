#!/bin/bash

#
# Name: select.sh
#
# Description: This script lists the FPGA available in the cluster, optionally
#  marks their current users and disable selection, or list FPGAs currently
#  locked by the calling user. User can gain/discharge exclusive access to the
#  FPGA by selecting the card. The script relies on the FPGA-JTAG mapping file,
#  at /<clustername>/<hostname>/etc/fpga and either,
#   1) /<clustername>/<hostname>/dev/        when mode set to "all"
#   2) /<clustername>/<hostname>/var/lock/   when mode set to "avail"
#   3) <caller_home>/.fpga/                  when mode set to "used"
#  to get a list of currently available FPGA 1) without or 2) with consideration
#  of lock status or 3) a list of caller-locked FPGAs. As 1) is populated by
#  system start up for attached JTAG, it presents the most update view of the
#  system JTAG hardware. Regardless whether a FPGA-JTAG mapping has been found
#  in the mapping file, the FPGA will be listed when found in 1), but the user
#  will be prompted to contact sysadmin to modify the mapping file.
#
#  The file presented in the 3 directories will be named "jtag_<esn>"
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

source /$CLUSTERNAME/opt/common.sh;

user=$(who am i | cut -d ' ' -f1);
home=$(getent passwd | grep $user | cut -d ':' -f6);

mode="avail";        # 'all' OR 'avail' OR 'used'

usage="Select existing/available/using FPGA
Usage: $0 ( 'all' | 'avail' | 'used' ) [ 'scan' | 'hwsrv' ]
 e.g.: $0 used scan";

#
# This defines the scope of FPGAs this script is presenting to users.
#  1) FPGA locally owned at the calling node    when scope set to "local"
#  2) FPGA owned by the complete cluster        when scope set to "domain"
#
if [ -z "$scope" ]; then
    scope="domain";   # 'local' OR 'domain'
fi;

if [ $# -ge 1 ]; then
    if [ $1 == "all" ] || [ $1 == "avail" ] || [ $1 == "used" ]; then
        mode=$1;
    else
        echo "$usage";

        exit 1;
    fi;
fi;

nact="acqrel";

if [ $# -gt 1 ]; then
    if [ $2 == "scan" ]; then
        nact=$2;
    elif [ $2 == "hwsrv" ]; then
        nact=$2;
    else
        echo "$usage";

        exit 1;
    fi;
fi;

#
# This defines if the script is presenting past usage to users. Default is "true".
#
if [ -z "$hist" ]; then
    hist="true";     # 'true' OR 'false'
fi;

if [ $mode == "used" ] && [ ! -e $home/.fpga ]; then
    echo "FAILED: No FPGA ever acquired";
    exit 1;
fi;

echo "Please Select the Following FPGA JTAG";

jtag_node=();

#
# The loop block exhausts member nodes in a cluster domain and prepare for arrays of
#  identifiers of FPGA, JTAG and lock files.
#
if [ $scope == "local" ]; then
    list_node=$host;
else
    list_node=$(ldapsearch -H $ldapsrv_prot://$ldapsrv -x -b "$ldap_basedn" -o ldif-wrap=no '(&(objectClass=realm)(cn='$clus'))' | grep "host:" | tr -s " " | cut --complement -d " " -f1);
fi;

for node in $list_node; do

    #
    # This loop block populates JTAG device files detected.
    #
    while read line; do
        jtag[${#jtag[@]}]="$line";

        # If the script is ran in "used" mode, the JTAG list is from user directory
        #  and unrecognisable as of a JTAG is from which member node of the cluster.
        #
        # This conditional block forbids the JTAG-associate node list to populate or
        #  causes incorrect matching.
        #
        if [ $mode != "used" ]; then
            jtag_node[${#jtag_node[@]}]="$node";
        fi;
    done < <(ls /$clus/$node/dev/);

    #
    # This loop block populates list of FPGA descriptions by reading administrator-
    #  prepared handmade description files.
    #
    while read line; do
        fdsc[${#fdsc[@]}]=$(echo $line | cut --complement -d " " -f1);
        fkey[${#fkey[@]}]=$(echo $line | cut -d " " -f1);

        fdsc_node[${#fdsc_node[@]}]="$node";
    done < <(cat /$clus/$node/etc/fpga);
done;

#
# This block is ran in "used" mode to populate a list of JTAG device files currently
#  acquired by the user.
#
if [ $mode == "used" ]; then
    jtag=(`ls $home/.fpga/`);
fi;

#
# This block handles FPGAs known to administrator as human-readable FPGA descriptor
#  entries will be added by administrator at board installation. The script will
#  match individual JTAG device with the human-readable FPGA descriptors entry.
#
# For boards that has no human-readable FPGA descriptor entry, they will
#  be presented as "UNKNOWN CARD" but still acquirable.
#
for jtag_i in `seq 1 ${#jtag[@]}`; do
    jtag_exist=0;

    for fkey_i in `seq 1 ${#fkey[@]}`; do
        if [ "${fkey[$((fkey_i-1))]}" = "${jtag[$((jtag_i-1))]}" ]; then
            jtag_exist=$fkey_i;
        fi;
    done;

    # If the script is ran in "used" mode, the JTAG list is from user directory
    #  and unrecognisable as of a JTAG is from which member node of the cluster.
    #
    # This conditional block matches back the cluster node that owns the JTAG.
    #
    if [ $mode == "used" ]; then
        jtag_node[${#jtag_node[@]}]="${fdsc_node[$((jtag_exist-1))]}";
    fi;

    #
    # In "domain" scope, this presents FPGAs a node is associated with.
    #
    if [ $scope == "domain" ] \
        && [ "$prev_node" != "${jtag_node[$((jtag_i-1))]}" ]; then
        echo "${jtag_node[$((jtag_i-1))]}:";
    fi;

    prev_node="${jtag_node[$((jtag_i-1))]}";

    if [ $jtag_exist -gt 0 ]; then
        if [ $mode == "avail" ] && [ -e /$clus/${jtag_node[$((jtag_i-1))]}/var/lock/${jtag[$((jtag_i-1))]}/lock ]; then
            if [ `cat /$clus/${jtag_node[$((jtag_i-1))]}/var/lock/${jtag[$((jtag_i-1))]}/lock` -eq 0 ] || ([ $nact == "scan" ] && [ `cat /$clus/${jtag_node[$((jtag_i-1))]}/var/lock/${jtag[$((jtag_i-1))]}/lock` -eq `grep $user <(getent passwd) | cut -d ':' -f3` ]); then
                echo "$jtag_i) ${fdsc[$(($jtag_exist-1))]}";

                if [ -e /$clus/${jtag_node[$((jtag_i-1))]}/etc/${jtag[$((jtag_i-1))]} ]; then
                    dvtp=`cat /$clus/${jtag_node[$((jtag_i-1))]}/etc/${jtag[$((jtag_i-1))]}`;
                    echo "            DEVICE TYPE: $dvtp";
                fi;
            else
                echo "X) ${fdsc[$(($jtag_exist-1))]}";

                if [ -e /$clus/${jtag_node[$((jtag_i-1))]}/etc/${jtag[$((jtag_i-1))]} ]; then
                    dvtp=`cat /$clus/${jtag_node[$((jtag_i-1))]}/etc/${jtag[$((jtag_i-1))]}`;
                    echo "            DEVICE TYPE: $dvtp";
                fi;

                if [ -e /$clus/${jtag_node[$((jtag_i-1))]}/var/lock/${jtag[$((jtag_i-1))]}/time ]; then
                    remt=$((`cat /$clus/${jtag_node[$((jtag_i-1))]}/var/lock/${jtag[$((jtag_i-1))]}/time`+$quota-`date +%s`));

                    echo "   ^ USED BY `getent passwd | grep $(cat /$clus/${jtag_node[$((jtag_i-1))]}/var/lock/${jtag[$((jtag_i-1))]}/lock) | cut -d ':' -f1` ($((remt/60)) Minutes $((remt % 60)) Seconds Remaining)";
                else
                    echo "   ^ USED BY `getent passwd | grep $(cat /$clus/${jtag_node[$((jtag_i-1))]}/var/lock/${jtag[$((jtag_i-1))]}/lock) | cut -d ':' -f1` (Unknown Remaining Time)";
                fi;
            fi;

            if [ $hist == "true" ] && [ ! $nact == "scan" ]; then
                hist_i=(`tail -n 5 /$clus/${jtag_node[$((jtag_i-1))]}/var/lock/${jtag[$((jtag_i-1))]}/lock_history`);

                for i in `seq 1 ${#hist_i[@]}`; do
                    echo "    $i-th Last: `getent passwd | grep ${hist_i[$((${#hist_i[@]}-i))]} | cut -d ':' -f1`";
                done;
            fi;
        else
            echo "$jtag_i) ${fdsc[$(($jtag_exist-1))]}";
        fi;
    else
        echo "$jtag_i) UNKNOWN CARD (CONTACT ADMINISTRATOR)";

        if [ -e /$clus/${jtag_node[$((jtag_i-1))]}/etc/${jtag[$((jtag_i-1))]} ]; then
            dvtp=`cat /$clus/${jtag_node[$((jtag_i-1))]}/etc/${jtag[$((jtag_i-1))]}`;
            echo "            DEVICE TYPE: $dvtp";
        fi;
    fi;
done;

#
# This block handles FPGAs known to administrator but currently missing from the
#  the system. The script will present JTAG device with the human-readable FPGA
#  descriptors entry but presented as "MISSING CARD" and not acquirable.
#
if [ ! $mode == "used" ]; then
    for fkey_i in `seq 1 ${#fkey[@]}`; do
        jtag_exist=0;

        for jtag_i in `seq 1 ${#jtag[@]}`; do
            if [ "${fkey[$((fkey_i-1))]}" == "${jtag[$((jtag_i-1))]}" ]; then
                jtag_exist=1;
            fi;
        done;

        if [ $jtag_exist -eq 0 ]; then
            echo "   MISSING CARD (CONTACT ADMINISTRATOR)";

            if [ -e /$clus/${jtag_node[$((jtag_i-1))]}/etc/${jtag[$((jtag_i-1))]} ]; then
                dvtp=`cat /$clus/${jtag_node[$((jtag_i-1))]}/etc/${jtag[$((jtag_i-1))]}`;
                echo "            DEVICE TYPE: $dvtp";
            fi;
        fi;
    done;
fi;

#
# If a valid JTAG device is selected from the selection menu, the script will
#  proceed on to acquire, release FPGA or rescan PCI-E endpoint an FPGA
#  associates with.
#
if [ ${#jtag[@]} -gt 0 ]; then
    echo -ne "\n> ";
    read select;

    echo "Selected FPGA JTAG #$select";

    if [ $mode == "avail" ] && [ -e /$clus/${jtag_node[$((select-1))]}/var/lock/${jtag[$((select-1))]}/lock ]; then
        if [ ! $nact == "scan" ] && [ `cat /$clus/${jtag_node[$((select-1))]}/var/lock/${jtag[$((select-1))]}/lock` -eq 0 ]; then
            ACT="acq";
        elif [ $nact == "scan" ] && ([ `cat /$clus/${jtag_node[$((select-1))]}/var/lock/${jtag[$((select-1))]}/lock` -eq 0 ] || [ `cat /$clus/${jtag_node[$((select-1))]}/var/lock/${jtag[$((select-1))]}/lock` -eq `grep $user <(getent passwd) | cut -d ':' -f3` ]); then
            ACT="scan";
        elif [ $nact == "hwsrv" ] && ([ `cat /$clus/${jtag_node[$((select-1))]}/var/lock/${jtag[$((select-1))]}/lock` -eq 0 ] || [ `cat /$clus/${jtag_node[$((select-1))]}/var/lock/${jtag[$((select-1))]}/lock` -eq `grep $user <(getent passwd) | cut -d ':' -f3` ]); then
            ACT="hwsrv"
        else
            echo "FAILED: Acquire FPGA ${jtag[$((select-1))]}, Acquired by `getent passwd | grep $(cat /$clus/${jtag_node[$((select-1))]}/var/lock/${jtag[$((select-1))]}/lock) | cut -d ':' -f1`";
            exit 1;
        fi;
    else
        if [ ! $mode == "used" ]; then
            if [ $nact == "scan" ]; then
                ACT="scan";
            elif [ $nact == "hwsrv" ]; then
                ACT="hwsrv";
            else
                ACT="acq";
            fi;
        else
            if [ $nact == "scan" ]; then
                ACT="scan";
            elif [ $nact == "hwsrv" ]; then
                ACT="hwsrv";
            else
                ACT="rel";
            fi;
        fi;
    fi;
else
    if [ ! $mode == "used" ]; then
        echo "FAILED: NO FPGA JTAG Available";
    else
        echo "FAILED: NO FPGA Ever Acquired";
    fi;

    exit 1;
fi;

case $ACT in
    acq)
        echo "Next Stage: Acquire ${jtag[$((select-1))]}";

        if [ $scope == "local" ] || ([ $scope == "domain" ] && [ ${jtag_node[$((select-1))]} == $host ]); then
            $proj/acquire.sh ${jtag[$((select-1))]};
        else
            # The sleep $((quota+30)) will keep the SSH session alive for the delay script to continue.
            #
            sudo -b -u $user ssh -t ${jtag_node[$((select-1))]} "sudo /opt/rm_acquire.sh ${jtag[$((select-1))]} && sleep $((quota+30))";
        fi;
        ;;

    scan)
        echo "Next Stage: Rescan PCI Slot ${jtag[$((select-1))]}";

        if [ $scope == "local" ] || ([ $scope == "domain" ] && [ ${jtag_node[$((select-1))]} == $host ]); then 
            $proj/rescan.sh ${jtag[$((select-1))]};
        else
            sudo -b -u $user ssh -t ${jtag_node[$((select-1))]} "sudo /opt/rm_rescan.sh ${jtag[$((select-1))]}";
        fi;
        ;;

    hwsrv)
        echo "Next Stage: Restart Hardware Server ${jtag[$((select-1))]}";

        if [ $scope == "local" ] || ([ $scope == "domain" ] && [ ${jtag_node[$((select-1))]} == $host ]); then 
            echo "INFO: Killing Running Hardware Servers";
            $proj/xilhwsrv.sh ${jtag[$((select-1))]} "kill";

            echo "INFO: Starting Hardware Servers";
            $proj/xilhwsrv.sh ${jtag[$((select-1))]} "start";
        else
            echo "INFO: Restarting Running Hardware Servers";
            sudo -b -u $user ssh -t ${jtag_node[$((select-1))]} "sudo /opt/rm_xilhwsrv.sh ${jtag[$((select-1))]} \"restart\"";
        fi;
        ;;

    rel)
        echo "Next Stage: Pre-Mature Release FPGA ${jtag[$((select-1))]}";

        if [ $scope == "local" ] || ([ $scope == "domain" ] && [ ${jtag_node[$((select-1))]} == $host ]); then
            $proj/chklock.sh ${jtag[$((select-1))]};
        else
            sudo -b -u $user ssh -t ${jtag_node[$((select-1))]} "sudo /opt/rm_chklock.sh ${jtag[$((select-1))]}";
        fi;
        ;;
esac;
