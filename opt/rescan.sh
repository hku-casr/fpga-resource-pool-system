#!/bin/sh

#
# Name: rescan.sh
#
# Description: This script rescans a PCI bus to re-discover a FPGA card after
#  reconfiguration.
#
#  The script relies on the FPGA lock directory and bus directory to locate an
#  appropriate bus to scan. The lock director is at
#   /<clustername>/<hostname>/var/lock/ and the bus directory is at
#   /<clustername>/<hostname>/sys/.
#
#  A user can only scan FPGA the user has locked or FPGA in public space
#  awaiting acquiring.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

if [ $# -eq 0 ]; then
    echo "Rescan a PCI Slot after FPGA reconfiguration";
    echo "Usage: $0 <jtag_esn>";
    echo " e.g.: $0 jtag_00001234567890";
    exit 1;
fi;

source /$CLUSTERNAME/opt/common.sh

base="/$clus/$host/var/lock";
bus=`readlink -sf $base/$1/pci_bus`;

flock -en $base/$1 -c "if [ ! -e $base/$1/lock ]; then echo '0' > $base/$1/lock; fi; if [ `cat $base/$1/lock` == '0' ] || [ `cat $base/$1/lock` == `id -u $(who am i | cut -d ' ' -f1)` ]; then echo 'Removing PCI Bus '$bus; echo 1 > $bus/remove; else echo 'FPGA Acquired by `getent passwd | grep $(cat $base/$1/lock) | cut -d ':' -f1`'; exit 1; fi;"

if [ $? -eq 0 ]; then
    echo "SUCCESS: Removed PCI Slot paired with $1";
else
    echo "FAILED: Remove PCI Slot paired with $1";
fi;

sleep 0.1;

echo 1 > "/sys/bus/pci/rescan";

echo "SUCCESS: Rescanned PCI Bus";
