#!/bin/bash

#
# Name: delaybdg.sh
#
# Description: This script bridges the Acquire script and the Release script by
#  idling a fixed amount of time to allow appropriate user time usage, default
#  is 15 minutes. The script will then move on to call the release script to
#  unlock the FPGA exclusive lock.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

if [ $# -eq 0 ]; then
    echo "Timer for fixed time usage, bridge the acquire and release scripts";
    echo "Usage: $0 <jtag_esn>";
    echo " e.g.: $0 jtag_00001234567890";
    exit 1;
fi;

source /$CLUSTERNAME/opt/common.sh

base="/$clus/$host/var/lock";
user=$(who am i | cut -d ' ' -f1)
upas=$user
start=$(date +%s);

function remain_second { echo $(($quota-($(date +%s)-$start))); };
function remain_time {
    remt=$(($quota-($(date +%s)-$start)));
    echo "$((remt/60)) Minutes $((remt % 60)) Seconds Remaining";
};

# FOR FUTURE EXTENSION OF HANDING OVER THE COUNTDOWN TO ANOTHER NODE IF SYSTEM
#  IS HALTING.
#
trap remain_second SIGUSR1;
trap remain_time SIGUSR2;

sleep $quota & # DEFAULT: 20m
cpid=$!;

while :; do
#    wait $!; # WILL NOT FULFILL CONDITION WHEN SLEEP IS KILLED INSTEAD OF
              #  PEACEFULLY EXIT
    if [ `ps -ef | grep "$cpid" | grep -v "grep" | tr -s " " | cut -d " " -f2 | wc -l` -eq 0 ]; then
        break;
    fi;
done;

echo "Next Stage: Release FPGA $1";

sudo -u $user sudo /opt/release.sh $1 $upas;