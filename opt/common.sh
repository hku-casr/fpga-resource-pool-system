#!/bin/bash

#
# Name: common.sh
#
# Description: This script defines common variables and establish common
#  environments for scripts under the FPGA resource pool reservation system.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

clus="americano";
host=`echo $HOSTNAME | cut -d "." -f1`;
proj="/$clus/opt";
quota=$((20*60));
scope="domain";
ldapsrv="bluemountain.eee.hku.hk";
ldapsrv_prot="ldaps";
ldap_basedn="dc=casr,dc=eee,dc=hku,dc=hk"
xilhwsrv_baseport=3121;
xilhwsrv_portbound=$((3121+30)); # UPPER BOUND, NON-INCLUSIVE