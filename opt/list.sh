#!/bin/bash

#
# Name: list.sh
#
# Description: This script lists the FPGA available in the cluster, and mark the
#  last 5 acquirers of the corresponding FPGA to allow users to have information
#  prior to acquistion of lock to avoid thrashing. The script also prints the
#  list of ports currently listened by Vivado Hardware Servers associated with
#  their corresponding FPGA.
#
#  The script relies on
#   1) /<clustername>/<hostname>/dev/        when mode set to "all"
#   2) /<clustername>/<hostname>/var/lock/   when mode set to "avail"
#  to get a list of currently available FPGA with consideration of lock status
#  and the historical acquirer of FPGAs.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

source /$CLUSTERNAME/opt/common.sh;

echo "FPGA Available in the cluster $clus and Last Acquirers";

for node in `ldapsearch -H $ldapsrv_prot://$ldapsrv -x -b "$ldap_basedn" -o ldif-wrap=no '(&(objectClass=realm)(cn='$clus'))' | grep "host:" | tr -s " " | cut --complement -d " " -f 1`; do
    jtag=(`ls /$clus/$node/dev/`);
    used="/$clus/$node/var/lock";
    dscp="/$clus/$node/etc";
    fkey=(`cat /$clus/$node/etc/fpga | cut -d " " -f1`);
    fdsc=();
    pfile="/$clus/$node/etc/xilhwsrv_port";

    while read line; do
        fdsc[${#fdsc[@]}]=$line;
    done < <(cat /$clus/$node/etc/fpga | cut --complement -d " " -f1);

    echo "$node:";

    for jtag_i in `seq 1 ${#jtag[@]}`; do
        jtag_exist=0;

        for fkey_i in `seq 1 ${#fkey[@]}`; do
            if [ "${fkey[$((fkey_i-1))]}" = "${jtag[$((jtag_i-1))]}" ]; then
                jtag_exist=$fkey_i;
            fi;
        done;

        if [ -e $used/${jtag[$((jtag_i-1))]}/lock_history ] && [ -e $used/${jtag[$((jtag_i-1))]}/lock ]; then
            if [ `cat $used/${jtag[$((jtag_i-1))]}/lock` -eq 0 ]; then
                if [ $jtag_exist -gt 0 ]; then
                    echo "  AVAILABLE ${fdsc[$(($jtag_exist-1))]}";
                else
                    echo "  AVAILABLE UNKNOWN CARD (CONTACT ADMINISTRATOR)";
                fi;

                if [ -e $dscp/${jtag[$((jtag_i-1))]} ]; then
                    dvtp=`cat $dscp/${jtag[$((jtag_i-1))]}`
                    echo "            DEVICE TYPE: $dvtp"
                fi;
            else
                if [ $jtag_exist -gt 0 ]; then
                    echo "  LOCKED    ${fdsc[$(($jtag_exist-1))]}";
                else
                    echo "  LOCKED    UNKNOWN CARD (CONTACT ADMINISTRATOR)";
                fi;

                if [ -e $dscp/${jtag[$((jtag_i-1))]} ]; then
                    dvtp=`cat $dscp/${jtag[$((jtag_i-1))]}`
                    echo "            DEVICE TYPE: $dvtp"
                fi;

                if [ -e $pfile ]; then
                    while read line; do
                        if [ `echo $line | cut -d ':' -f1` == "${jtag[$((jtag_i-1))]}" ]; then
                            vver=$(echo $line | cut -d ':' -f2);
                            port=$(echo $line | cut -d ':' -f3);

                            if [ `ss -l "( sport = :$port )" | grep "LISTEN" | wc -l` -gt 0 ]; then
                                echo "              Vivado Hardware Server $vver | $host:$port";
                            fi;
                        fi;
                    done < $pfile;
                fi;

                if [ -e $used/${jtag[$((jtag_i-1))]}/time ]; then
                    remt=$((`cat $used/${jtag[$((jtag_i-1))]}/time`+$quota-`date +%s`));

                    echo "  `getent passwd | grep $(cat $used/${jtag[$((jtag_i-1))]}/lock) | cut -d ':' -f1` ($((remt/60)) Minutes $((remt % 60)) Seconds Remaining)";
                else
                    echo "  `getent passwd | grep $(cat $used/${jtag[$((jtag_i-1))]}/lock) | cut -d ':' -f1` (Unknown Remaining Time)";
                fi;
            fi;

            hist_i=(`tail -n 5 $used/${jtag[$((jtag_i-1))]}/lock_history`);

            for i in `seq 1 ${#hist_i[@]}`; do
                echo "              $i-th Last: `getent passwd | grep ${hist_i[$((${#hist_i[@]}-i))]} | cut -d ':' -f1`";
            done;
        else
            if [ $jtag_exist -gt 0 ]; then
                echo "  UNKNOWN   ${fdsc[$(($jtag_exist-1))]}";
            else
                echo "  UNKNOWN   UNKNOWN CARD (CONTACT ADMINISTRATOR)";
            fi;

            if [ -e $dscp/${jtag[$((jtag_i-1))]} ]; then
                dvtp=`cat $dscp/${jtag[$((jtag_i-1))]}`
                echo "            DEVICE TYPE: $dvtp"
            fi;
        fi;
    done;

    for fkey_i in `seq 1 ${#fkey[@]}`; do
        jtag_exist=0;

        for jtag_i in `seq 1 ${#jtag[@]}`; do
            if [ "${fkey[$((fkey_i-1))]}" == "${jtag[$((jtag_i-1))]}" ]; then
                jtag_exist=1;
            fi;
        done;

        if [ $jtag_exist -eq 0 ]; then
            echo "             MISSING CARD (CONTACT ADMINISTRATOR)";
        fi;
    done;
done;
