#!/bin/bash

#
# Name: release.sh
#
# Description: This script Release a lock associated with the JTAG interface of
#  a FPGA after an exclusive use. The script will also reset the file permission
#  of the JTAG device file back to permission 000 to shield from user access.
#
#  The script will release the lock by resetting the iptables to block packets
#  intended for the Xilinx Vivado Hardware Server again and reset the lock
#  file. Parallel to the Acquire script, this script will set all iptables of
#  every node within the domain if scope is set to "domain" or set local
#  iptable if scope is set to "local". The lock file can be located in the JTAG
#  associated lock directory, i.e.,
#  /<clustername>/<hostname>/var/lock/<jtag_esn>/lock.
#
#  The script will remove the temporary file stored in the home directory of a
#  user, at ~/.fpga/.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

if [ $# -eq 0 ]; then
    echo "Release an FPGA exclusive lock, kills hardware server if the file is still opening";
    echo "Usage: $0 <jtag_esn> [<user_id>]";
    echo " e.g.: $0 jtag_00001234567890 admin";
    exit 1;
elif [ $# -eq 1 ]; then
    user=$(who am i | cut -d ' ' -f1);
else
    user=$2;

    acct=0;

    for i in `getent passwd | cut -d ":" -f1`; do
        if [ $i == $2 ]; then
            acct=1;
        fi
    done;

    if [ $acct -eq 0 ]; then
        echo "FAIL: Invalid User Account to Release FPGA $1"
        exit 1;
    fi;
fi;

source /$CLUSTERNAME/opt/common.sh

base="/$clus/$host/var/lock";
home=$(getent passwd | grep $user | cut -d ':' -f6);
pfile="/$clus/`echo $HOSTNAME | cut -d "." -f1`/etc/xilhwsrv_port";

if [ -z "$scope" ]; then
    scope="domain";   # 'local' OR 'domain'
fi;

if [ $scope == "local" ]; then
    list_node=$host;
else
    list_node=$(ldapsearch -H $ldapsrv_prot://$ldapsrv -x -b "$ldap_basedn" -o ldif-wrap=no '(&(objectClass=realm)(cn='$clus'))' | grep "host:" | tr -s " " | cut --complement -d " " -f1);
fi;

flock -e $base/$1 -c "if [ ! -e $base/$1/lock ]; then echo '0' > $base/$1/lock; exit 0; fi; if [ `cat $base/$1/lock` != `id -u $user` ]; then exit 1; fi";

cmd=":";

if [ $? -eq 0 ]; then
    while read line; do
        if [ `echo $line | cut -d ':' -f1` == "$1" ]; then
            vver=$(echo $line | cut -d ':' -f2);
            port=$(echo $line | cut -d ':' -f3);

            echo "INFO: Closing Access to Vivado Hardware Server Version $vver at Port $port";

            cmd="$cmd && iptables -D frps-rules-output -d `getent hosts | grep $host | cut -d \" \" -f1 | head -n 1` -m owner --uid-owner $user -p tcp --dport $port -j ACCEPT";
        fi;
    done < $pfile;

    for i in $list_node; do
        if [ "$i" != "$host" ]; then
            ssh -t $i "$cmd";
        else
            eval "$cmd";
        fi;
    done;

    flock -e $base/$1 -c "cat $base/$1/lock >> $base/$1/lock_history; echo '0' > $base/$1/lock; rm -f $base/$1/time";

    rm -f $home/.fpga/$1;

    echo "SUCCESS: Released FPGA $1";
else
    echo "FAIL: Release FPGA $1";
    exit 1;
fi;
