#!/bin/bash

#
# Name: acquire.sh
#
# Description: This script Acquire a lock associated with the JTAG interface of
#  a FPGA for exclusive use. The script will move on to call a delay script to
#  allow appropriate time for such exclusive use before automatically calling
#  the Release script to release the FPGA lock back to the pool for acquiring.
#  The default delay is 20 minutes and is specified in the script called next
#  by the current script.
#
#  The script will acquire the lock by allow packets going to Xilinx Vivado
#  Hardware Server to pass by setting the corresponding iptables entry.
#  Depending on the scope set for the script, the script will set all iptables
#  of every node within the domain if scope is set to "domain" or set local
#  iptable if scope is set to "local". The script will also change the lock
#  file. The lock file can be located in the JTAG associated lock directory,
#  i.e., /<clustername>/<hostname>/var/lock/<jtag_esn>/lock.
#
#  The script will touch a temporary file with the name of "jtag_<esn>" to be
#  stored in the home directory of the calling user, at ~/.fpga/. The .fpga
#  directory will be created if not present.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

if [ $# -eq 0 ]; then
    echo "Acquire an FPGA exclusive lock, return non-zero if lock operation fail";
    echo "Usage: $0 <jtag_esn>";
    echo " e.g.: $0 jtag_00001234567890";
    exit 1;
fi;

source /$CLUSTERNAME/opt/common.sh

base="/$clus/$host/var/lock";
wscp="/opt/delay.sh";
user=$(who am i | cut -d ' ' -f1);
home=$(getent passwd | grep $user | cut -d ':' -f6);
pfile="/$clus/`echo $HOSTNAME | cut -d "." -f1`/etc/xilhwsrv_port";

if [ -z "$scope" ]; then
    scope="domain";   # 'local' OR 'domain'
fi;

if [ $scope == "local" ]; then
    list_node=$host;
else
    list_node=$(ldapsearch -H $ldapsrv_prot://$ldapsrv -x -b "$ldap_basedn" -o ldif-wrap=no '(&(objectClass=realm)(cn='$clus'))' | grep "host:" | tr -s " " | cut --complement -d " " -f1);
fi;

flock -en $base/$1 -c "if [ ! -e $base/$1/lock ]; then echo '0' > $base/$1/lock; fi; if [ `cat $base/$1/lock` == '0' ]; then echo `id -u $user` > $base/$1/lock; echo `date +%s` > $base/$1/time; else echo 'Lock Operation Failed'; echo 'FPGA Acquired by `getent passwd | grep $(cat $base/$1/lock) | cut -d ':' -f1`'; exit 1; fi";

if [ $? -eq 0 ]; then
    if [ ! -e $home/.fpga ]; then
        sudo -u $user mkdir $home/.fpga;
    fi;

    sudo -u $user touch $home/.fpga/$1;

    cmd=":";

    while read line; do
        if [ `echo $line | cut -d ':' -f1` == "$1" ]; then
            vver=$(echo $line | cut -d ':' -f2);
            port=$(echo $line | cut -d ':' -f3);

            if [ `ss -l "( sport = :$port )" | grep "LISTEN" | wc -l` -gt 0 ]; then
                echo "INFO: Vivado Hardware Server Version $vver is Listening at $host:$port";

                cmd="$cmd && iptables -I frps-rules-output -d `getent hosts | grep $host | cut -d \" \" -f1 | head -n 1` -m owner --uid-owner $user -p tcp --dport $port -j ACCEPT";
            fi;
        fi;
    done < $pfile;

    for i in $list_node; do
        if [ "$i" != "$host" ]; then
            ssh -t $i "$cmd";
        else
            eval "$cmd";
        fi;
    done;

    echo "SUCCESS: Acquired FPGA $1";
    echo "Next Stage: Time FPGA $1";

    sudo -b -u $user sudo $wscp $1 2>&1
else
    echo "FAIL: Acquire FPGA $1";
    exit 1;
fi;
