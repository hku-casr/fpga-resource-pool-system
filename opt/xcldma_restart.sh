#!/bin/bash

#
# Name: xcldma_restart.sh
#
# Description: This script uninstall and re-install the Xilinx OpenCL Direct
#  Memory Access (xcldma) driver from/into the kernel for a de facto reboot of
#  the driver.
#
#  NOTE: The script acts as an ad-hoc problem fix for now and does not consider
#   elaborate checking like FPGA lock ownership test before execution. Should
#   the script be permenently in-place, execution permission check should be
#   conducted or risks conflict in multi-user environment for pulling the DMA
#   module from in-action use by other single users.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

sudo rmmod xcldma && sudo modprobe xcldma

if [ $? -eq 0 ]; then
    echo "INFO: RESTARTED xcldma";

    lsmod | grep xcldma;
else
    echo "FAILED: CANNOT RESTART xcldma";

    exit 1;
fi;