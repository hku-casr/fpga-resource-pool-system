#!/bin/bash

#
# Name: chklock.sh
#
# Description: This script kills the `sleep` sub-process of the Acquire-Release
#  cycle bridge script, so that the script will continue to move on to call the 
#  release script.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

if [ $# -eq 0 ]; then
    echo "Check if an FPGA exclusive lock has been effectively acquired, kill the Acquire-Lock cycle delay script to prevent double release";
    echo "Usage: $0 <jtag_esn>";
    echo " e.g.: $0 jtag_00001234567890";
    exit 1;
fi;

source /$CLUSTERNAME/opt/common.sh

base="/$clus/$host/var/lock";
wscp="/opt/delay.sh";

# Handling cases where the script is triggered by a remote process.
#
if [ -z "$user" ]; then
    user=$(who am i | cut -d ' ' -f1);
fi

flock -en $base/$1 -c "if [ -e $base/$1/lock ] && [ `cat $base/$1/lock` == `id -u $(who am i | cut -d ' ' -f1)` ]; then pkill -KILL -P `ps -ef --sort=start_time | grep "/bin/bash $wscp $1" | grep -v "grep" | tr -s ' ' | cut -d ' ' -f2 | tail -n 1`; fi";

if [ ! $? -eq 0 ]; then
    sudo -u $user sudo /opt/release.sh $1
fi;
