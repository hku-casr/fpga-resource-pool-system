#!/bin/bash

#
# Name: install.sh
#
# Description: This script spawns shortcuts in the optional software directory,
#  i.e. /opt of a local system, that link to FPGA Resource Pool System user-
#  interfacing scripts that reside in the FRPS installation directory.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

if [ -z "$INSTALL_DIR" ]; then
    echo "ERROR: Environment variable INSTALL_DIR not found";

    exit 1;
fi;

ln -sf /$INSTALL_DIR/opt/delaybdg.sh /opt/delay.sh
ln -sf /$INSTALL_DIR/opt/list.sh /opt/list_fpga.sh
ln -sf /$INSTALL_DIR/opt/lock.sh /opt/lock_fpga.sh
ln -sf /$INSTALL_DIR/opt/unlock.sh /opt/unlock_fpga.sh
ln -sf /$INSTALL_DIR/opt/pci_rescan.sh /opt/pci_rescan.sh
ln -sf /$INSTALL_DIR/opt/hwsrv_restart.sh /opt/hwsrv_restart.sh
ln -sf /$INSTALL_DIR/opt/xcldma_restart.sh /opt/xcldma_restart.sh
ln -sf /$INSTALL_DIR/opt/acquire.sh /opt/rm_acquire.sh
ln -sf /$INSTALL_DIR/opt/release.sh /opt/release.sh
ln -sf /$INSTALL_DIR/opt/chklock.sh /opt/rm_chklock.sh
ln -sf /$INSTALL_DIR/opt/xilhwsrv.sh /opt/rm_xilhwsrv.sh
ln -sf /$INSTALL_DIR/opt/rescan.sh /opt/rm_rescan.sh
ln -sf /$INSTALL_DIR/etc/sudoers.d/frps /etc/sudoers.d/frps