#!/bin/bash

#
# Name: preplock.sh
#
# Description: This script prepares the lock facility that allow users to gain
#  exclusive access to FPGA. The script is designed to be ran at bootup, and
#  must be ran after the JTAG scan script.
#
#  The script prepares the lock facility, located at
#   /<clustername>/<hostname>/var/lock/<jtag_esn>/.
#
#  Each JTAG sub-directory will contains
#   1) lock, a file that marks a lock holder uid
#   2) lock_history, a file that keeps the last 5 holders of the lock
#   3) usb_jtag, a soft link that indirectly points to the JTAG device file
#   4) pci_bus, a soft link that indirectly points to the PCI Bus device
#       directory
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

source /$clus/opt/common.sh;

baseport=$xilhwsrv_baseport;
portbound=$xilhwsrv_portbound;

if [ -z "$scope" ]; then
    scope="domain";   # 'local' OR 'domain'
fi;

if [ $scope == "local" ]; then
    list_node=$host;
else
    list_node=$(ldapsearch -H $ldapsrv_prot://$ldapsrv -x -b "$ldap_basedn" -o ldif-wrap=no '(&(objectClass=realm)(cn='$clus'))' | grep "host:" | tr -s " " | cut --complement -d " " -f1);
fi;

for i in ${list_node[@]}; do
    list_ip[${#list_ip[@]}]=`getent hosts | grep $i | cut -d " " -f1`;
done;

if [ ! -e /$clus/$host/var/lock ]; then mkdir -p /$clus/$host/var/lock; fi;

for i in `ls /$clus/$host/dev/`; do
    rm -rf /$clus/$host/var/lock/$i;

    mkdir /$clus/$host/var/lock/$i;
    echo "0" > /$clus/$host/var/lock/$i/lock;
    touch /$clus/$host/var/lock/$i/lock_history;
    ln -s /$clus/$host/dev/$i /$clus/$host/var/lock/$i/usb_jtag;
    ln -s /$clus/$host/sys/$i /$clus/$host/var/lock/$i/pci_bus;

    chmod 000 /$clus/$host/var/lock/$i/usb_jtag;
done;

echo "Next Stage: Remove FRPS iptables rules of previous session";

# REMOVE FRPS IPTABLES RULES OF PREVIOUS SESSION
#
function del-rules {
    while read line; do
        cmd=$(echo $line | sed "s/-A/-D/g" | sed "s/-I/-D/g" | sed "s/-N/-X/g");

        eval "iptables $cmd";
    done <(iptables --list-rules $1 | grep $2 | tac);
}

del-rules INPUT frps-rules-input;
del-rules OUTPUT frps-rules-output;
del-rules frps-rules-input "";
del-rules frps-rules-output "";

echo "Next Stage: Initialise FRPS iptables rules";

# SET FRPS IPTABLES RULES
#
iptables -N frps-rules-input;

for i in ${list_ip[@]}; do
    iptables -A frps-rules-input -s $i -p tcp -m tcp --dport $baseport:$((portbound-1)) -j ACCEPT;
done;

iptables -A frps-rules-input -j REJECT;
iptables -I INPUT -p tcp --dport $baseport:$((portbound-1)) -j frps-rules-input;

iptables -N frps-rules-output;
iptables -A frps-rules-output -j REJECT;
iptables -I OUTPUT -p tcp --dport $baseport:$((portbound-1)) -j frps-rules-output;

# START VIVADO HARDWARE SERVER FOR INDIVIDUAL FPGA
#
for i in `ls /$clus/$host/dev/`; do
    clus=$clus /$clus/opt/xilhwsrv.sh $i "start";
done;