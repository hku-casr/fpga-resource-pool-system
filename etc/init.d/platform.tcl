#
# Name: targets.tcl
#
# Description: This script will find the FPGA Type of the FPGA connecting JTAG
#  device by Xilinx Vivado.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

open_hw
connect_hw_server
current_hw_target [lindex [get_hw_targets] 0]
open_hw_target
set dev [get_hw_devices]
set dsc "FOUND_DEVICE"
puts "$dsc:$dev"
quit
