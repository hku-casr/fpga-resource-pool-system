#!/bin/bash

#
# Name: jtag.sh
#
# Description: This script scans the FPGA JTAG available in the local host. The
#  script will find the Electronic Serial Number of the JTAG device by Xilinx
#  Vivado and create a file for at /<clustername>/<hostname>/dev/ to signify
#  the discovery. Files created will be named in format "jtag_<esn>" and are
#  soft links that point to the USB device file in the local virtual file
#  system.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

clus="americano";

source /$clus/opt/common.sh;

base="/dev/bus/usb";
xilv="/opt/Xilinx/Vivado/2015.4/bin";
list=`lsusb -d 03fd:0008 | cut -d ":" -f1 | cut -d " " -f2,4 --output-delimiter="/"`;

if [ ! -e /$clus/`echo $HOSTNAME | cut -d "." -f1` ]; then
    mkdir /$clus/`echo $HOSTNAME | cut -d "." -f1`;
fi;

for i in $list; do chmod 000 $base/$i; done;

baseport=$xilhwsrv_baseport;

rm -f /$clus/`echo $HOSTNAME | cut -d "." -f1`/etc/network_port;

iptables -I INPUT -s 127.0.0.1 -p tcp --dport $baseport -j ACCEPT;
iptables -I OUTPUT -p tcp --dport $baseport -j ACCEPT;

for i in $list; do
    chmod 666 $base/$i;

    sudo -u nobody -g nobody $xilv/unwrapped/lnx64.o/hw_server -d -s "tcp::$baseport" -I2 > /dev/null &

    sleep 1;

    tfile=`mktemp`;
    pfile=`mktemp`;

    sudo -u nobody -g nobody $xilv/vivado -mode batch -source /$clus/etc/init.d/targets.tcl -nolog -nojournal  > $tfile \
    && sudo -u nobody -g nobody $xilv/vivado -mode batch -source /$clus/etc/init.d/platform.tcl -nolog -nojournal > $pfile;

    if [ $? -ne 0 ]; then
        echo "ERROR: Query failed, will drop";

        rm -f $tfile $pfile;

        iptables -D INPUT -s 127.0.0.1 -p tcp --dport $baseport -j ACCEPT;
        iptables -D OUTPUT -p tcp --dport $baseport -j ACCEPT;

        exit 1;
    fi;

    esn=`cat $tfile | grep "^localhost:3121" | rev | cut -d "/" -f1 | rev`;
    dev=`cat $pfile | grep "FOUND_DEVICE:" | cut -d ':' -f2`;
    echo "FOUND: USB Device $i, JTAG ESN $esn";

    rm -f $tfile $pfile;

    if [ -z "$esn" ] || [ -z "$dev" ]; then
        echo "ERROR: Invalid parse, will drop";

        iptables -D INPUT -s 127.0.0.1 -p tcp --dport $baseport -j ACCEPT;
        iptables -D OUTPUT -p tcp --dport $baseport -j ACCEPT;

        exit 1;
    fi;

    if [ ! -e /$clus/`echo $HOSTNAME | cut -d "." -f1`/dev ]; then
        mkdir /$clus/`echo $HOSTNAME | cut -d "." -f1`/dev;
    fi;

    ln -sf $base/$i /$clus/`echo $HOSTNAME | cut -d "." -f1`/dev/jtag_$esn;
    echo $dev > /$clus/`echo $HOSTNAME | cut -d "." -f1`/etc/jtag_$esn;

    kill -s 9 `ps -ef | grep "$xilv/unwrapped/lnx64.o/hw_server" | grep -v "grep" | tr -s " " | cut -d " " -f2`;

    chmod 000 $base/$i;

    sleep 3;
done;

iptables -D INPUT -s 127.0.0.1 -p tcp --dport $baseport -j ACCEPT;
iptables -D OUTPUT -p tcp --dport $baseport -j ACCEPT;

for i in $list; do chmod 666 $base/$i; done;

echo "Next Stage: Start Xilinx Vivado Hardware Servers";

clus=$clus /$clus/etc/init.d/preplock.sh;
