#
# Name: targets.tcl
#
# Description: This script will find the Electronic Serial Number of the JTAG
#  device by Xilinx Vivado.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

open_hw
connect_hw_server
puts [get_hw_targets]
quit
